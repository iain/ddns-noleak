# Overview

*ddns-noleak*: Update DDNS records without leaking requests over plaintext, or the wrong interface.

# Explanation and Motivation

There are quite a few Dynamic DNS (DDNS) clients already. However, most of them send unencrypted DNS requests. Instead, we use DNS-over-HTTPS (DoH).

Moreover, most DDNS clients don’t support controlling the interface the client binds to (sends requests from). These clients could leak, over your VPN, requests or information about your non-VPN IP address, or vice versa. Whilst obviously your VPN provider needs to know your non-VPN IP address, neither your DDNS provider, nor a website which tells you your WAN IP address, needs to.

# Scope

Unfortunately, for simplicity, *ddns-noleak* only supports IPv4. Adding IPv6 would be straightforward, but has not been done.

Also for simplicity, the only DDNS provider supported is Cloudflare.

# Usage

Run the shell script *ddns-noleak* with the argument “--help”. You will be shown a list of the required parameters, and their explanation.

There is no retry logic, nor is this a daemon. On OpenWrt, if you use DHCP to access the internet, then place a script inside /etc/udhcpc.user.d/ to call *ddns-noleak*. Otherwise, you should schedule this to run at reasonable intervals. For example, on startup and every half hour.
